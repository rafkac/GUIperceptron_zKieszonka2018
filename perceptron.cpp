#include "perceptron.h"
#include "przyklad.h"

/**
 * @author Szogun
 * 8 października 2018
 * 4 listopada 2018
 *
 * Klasa ta jest pojedyńczym perceptronem.
 *
 * Algorytm z wykładu dotyczy pojedyńczego perceptronu - zakładamy, że każdy preceptron
 * jest uczony indywidualnie - tzn najpierw uczymy rozpoznawać liczby perceptron od 0, później od 1,
 * etc.
 *
 * Algorytm dla metody głównej tej klasy (a własciwie całej klasy):
 *  1 - w losujemy w konstruktorze wagi pierwotne,
 *  2 - wybieramy .losowy przykład E^j, zapamiętujemy, który to był przykład,
 *  3 - i odpowiadającą mu odpowiedź T^j (właściwie jest to 0, jeśli to ta liczba i 1, jeśli nie ta liczba),
 *  4 - obliczamy O - wynik dzialania sieci na E^j,
 *  5 - obliczamy ERR = T^j - O,
 *  6 - jeśli ERR = 0 (klasyfikacja poprawna) - to wracamy do 2,
 *  7 - w.p.p. uaktualniamy wagi zgodnie ze wzorem:
 *          w_i = w_i + ni * ERR *E_i^j,
 *          theta = theta - ERR,
 *      gdzie ni jest stałą uczenia, większą od 0, trzeba ją dobrze zadeklarować,
 *  8 - jeśli sięć klasyfikuje poprawnie wszystkie przykłady, to kończymy, wpp wracamy do 2.
 *-
 * Jest to właściwie przerobiony mój kod napisany w javie.
 * */

Perceptron::Perceptron(){
//    srand(time(NULL));
    tablicaWag = QVector<double>();
    for(int i = 0; i < 35; i++){
        double waga = 2.0 * ((double)rand()/ RAND_MAX) - 1;
        tablicaWag.push_back(waga);
    }
    this->n = 0;
    this->theta = 0.1;
    this->ERR = 0;
    this->wynikDzialaniaSieci = 0;
    this->stalaUczenia = 0.1;
    this->czyPrzykladJestTaLiczba = 0;
}

Perceptron::Perceptron(int liczba){
//    srand(time(NULL));
    tablicaWag = QVector<double>();
    for(int i = 0; i < 35; i++){
        double waga = 2.0 * ((double)rand()/ RAND_MAX) - 1;
        tablicaWag.push_back(waga);
//        qDebug("waga %d: %f", i, waga);
    }
    this->n = liczba;
    this->theta = 0.01;
    this->ERR = 0;
    this->wynikDzialaniaSieci = 0;
    this->stalaUczenia = 0.005;
    this->czyPrzykladJestTaLiczba = 0;
}

void Perceptron::setN(int n){
    this->n = n;
}

int Perceptron::getN(){
    return n;
}

void Perceptron::setTheta(double theta){
    this->theta = theta;
}

double Perceptron::getTheta(){
    return theta;
}

int Perceptron::getErr(){
    return ERR;
}

void Perceptron::setErr(int ERR){
    this->ERR = ERR;
}

void Perceptron::wartoscERR(double T_j){
    double pomoc;
    if(T_j == n){
        pomoc = 1;
    }
    else{
        pomoc = -1;
    }
    ERR = pomoc - wynikDzialaniaSieci;
}

void Perceptron::setStalaUczenia(double stalaUczenia){
    this->stalaUczenia = stalaUczenia;
}

double Perceptron::getStalaUczenia(){
    return stalaUczenia;
}

void Perceptron::setCzyPrzykladJestTaLiczba(int i){
    czyPrzykladJestTaLiczba = i;
}

int Perceptron::getCzyPrzykladJestTaLiczba(){
    return czyPrzykladJestTaLiczba;
}

void Perceptron::setWynikDzialaniaSieci(int wynikDzialaniaSieci){
    this->wynikDzialaniaSieci = wynikDzialaniaSieci;
}

int Perceptron::getWynikDzialaniaSieci(){
    return wynikDzialaniaSieci;
}

void Perceptron::setTablicaWag(QVector<double> tablicaWag){
    this->tablicaWag = tablicaWag;
}

QVector<double> Perceptron::getTablicaWag(){
    return tablicaWag;
}

int Perceptron::coJestNaWyjsciu(Przyklad wektor){
    double suma = -1 * theta;
//    qDebug("Rozmiar tablicy wag to: %d", tablicaWag.size());
    for(int i = 0; i < tablicaWag.size(); i++){
        suma += tablicaWag.at(i) * wektor.getMiejsce(i);
    }
    if(suma >= 0){
        wynikDzialaniaSieci = 1;
    }
    else{
        wynikDzialaniaSieci = -1;
    }
    return (int) wynikDzialaniaSieci;
}


/**
 * Poniższa metoda odpowiada za wykonanie całości - wszystkich kroków dla pojedyńczego perceptrona.
 * Czyli może zostać użyta zamiast wczesniejszych metod, tylko ona.
 *
 * Zwraca 0, jeśli dobrze sklasyfikowano dany przykład.
 * Zwraca 100, jeśli źle sklasyfikowano dany przykład (czyli przykład był x i wykryto nie x,
 * lub przykład był nie x w wykryto x).
 */
void Perceptron::wszystkieOperacje(Przyklad wektor){
    double suma = -1 * theta;
    QVector<double> localTablicaWag = getTablicaWag();
    int czyToTaCyfra = 0;
    int cyfraJakaJestPrzyklad = wektor.getJakaJestCyfra();

    for(int i = 0; i < localTablicaWag.size(); i++){
        suma += localTablicaWag.at(i) * wektor.getMiejsce(i);
    }
    if(suma >= 0){
        setWynikDzialaniaSieci(1);
    }
    else{
        setWynikDzialaniaSieci(-1);
    }
//    qDebug("suma: %f, wynik działania sieci: %d, getter: %d", suma, wynikDzialaniaSieci, getWynikDzialaniaSieci());
    // liczymy błąd perceptronu na danym przykładzie
    if(cyfraJakaJestPrzyklad == getN()){
        // rozpoznaliśmy w cyfrze cyfry, które rozpoznaje ten perceptron
        czyToTaCyfra = 1;
    }
    else{
        czyToTaCyfra = -1;
    }
    setCzyPrzykladJestTaLiczba(czyToTaCyfra);
//    ERR = czyToTaCyfra - wynikDzialaniaSieci;
    setErr(czyToTaCyfra - getWynikDzialaniaSieci());
    // niżej są aktywności, które nie powinny być w tej metodziez
    qDebug("Rozpatrywana: %d, Czy to ta cyfra? %d, ERR: %d, wynik działania sieci: %d",cyfraJakaJestPrzyklad,czyToTaCyfra, getErr(), getWynikDzialaniaSieci());
/*    if(getErr() == 0){
        qDebug("Poprawnie sklasyfikowaliśmy ten przykład. Jest to: %d, sklasyfikowaliśmy: %d", cyfraJakaJestPrzyklad, n);
        return 0;
    }
    else{
        qDebug("Źle sklasyfikowany przykład - uaktualniamy wagi.");
//        wypiszTabliceWag();
/*        for(int i = 0; i < localTablicaWag.size(); i++){
            qDebug("%f", localTablicaWag.at(i));
        }*/
//        uaktualnijWagi();                 // uaktualniamy wagi
/*        for(int i=0; i< localTablicaWag.size(); i++){
            double wpis = localTablicaWag.at(i);
            qDebug("Przed uaktualnieniem: %f, err: %d, czy przyklad jest ta liczba: %d", wpis, getErr(), czyToTaCyfra);
            wpis = wpis + getStalaUczenia() * (double)getErr() * (double) czyToTaCyfra;
            localTablicaWag[i]=wpis;
            qDebug("Po zmianie: %f", wpis);
            setTheta(getTheta() - (double)getErr() * (double)getStalaUczenia() );
        }
        setTablicaWag(localTablicaWag);
/*        qDebug("Wagi po uaktualnieniu");
        for(int i = 0; i < localTablicaWag.size(); i++){
            qDebug("%f", localTablicaWag.at(i));
        }*/
    //        wypiszTabliceWag();
}

/**
 * Metoda powyższa, ale w bardziej estetycznej postaci.
 * Robi dokładnie to samo, ale nie ma zakomentowanych linii powstałych we wcześniejszych etapach pracy twórczej.
 */
void Perceptron::wszystkieOperacje2(Przyklad wektor){
    double suma = -1 * this->getTheta();
    QVector<double> localTablicaWag = this->getTablicaWag();
    int czyToTaCyfra = 0;
    int cyfraJakaJestPrzyklad = wektor.getJakaJestCyfra();

//    qDebug("Suma na początku: %f", suma);
    for(int i = 0; i < localTablicaWag.size(); i++){
        suma = suma + (double)localTablicaWag.at(i) * (double)wektor.getMiejsce(i);
//        qDebug("%d", wektor.getMiejsce(i));
//        qDebug("waga: %f, miejsce: %d, suma: %f", localTablicaWag.at(i), wektor.getMiejsce(i), suma);
    }
//    qDebug("\n Suma = %f", suma);
    if(suma >= 0.0){
        setWynikDzialaniaSieci(1);
    }
    else{
        setWynikDzialaniaSieci(-1);
    }
    if(cyfraJakaJestPrzyklad == getN()){
//        qDebug("Spełniony warunek - rozpoznajemy cyfrę, którą powinniśmy umieć.");
        czyToTaCyfra = 1;
    }
    else{
        czyToTaCyfra = -1;
    }
    setCzyPrzykladJestTaLiczba(czyToTaCyfra);
    setErr(czyToTaCyfra - getWynikDzialaniaSieci());
//    qDebug("Rozpatrywana: %d, Czy to ta cyfra? %d, ERR: %d, wynik działania sieci: %d",cyfraJakaJestPrzyklad,czyToTaCyfra, getErr(), getWynikDzialaniaSieci());
}

/*
void Perceptron::uaktualnijWagi(){
//    qDebug("Uaktualniamy wagi.");
    //double wpis = 0;
    wypiszTabliceWag();
    for(int i = 0; i < 35; i++){
        double wpis = tablicaWag.at(i);
//        qDebug("Uaktualniamy %d wagę: %f", i, wpis);
        wpis += stalaUczenia * (double)ERR * (double)czyPrzykladJestTaLiczba;
        //tablicaWag.insert(i, wpis);
        tablicaWag[i] = wpis;
        theta = theta - (double)ERR * (double)stalaUczenia;
    }
    wypiszTabliceWag();
}*/

void Perceptron::wypiszTabliceWag(){
    qDebug("Rozmiar tablicy wag: %d", tablicaWag.size());
    for(int i = 0; i < tablicaWag.size(); i++){
        qDebug("%f", tablicaWag.at(i));
    }
}

/**
 * Poprawna metoda służąca uaktualnianiu wag - ta wcześniejsza jest błędna, bo pracuje na kopiach danych,
 * zatem nie uaktualnia ich po wykonaniu operacji.
 *
 * 10 listopada 2018
 */
void Perceptron::uaktualnijWagi(Przyklad pp){
    QVector<double> localTablicaWag = getTablicaWag();
    for(int i=0; i< localTablicaWag.size(); i++){
        double wpis = localTablicaWag.at(i);
//        qDebug("Przed uaktualnieniem: %f, err: %d, czy przyklad jest ta liczba: %d", wpis, getErr(), getCzyPrzykladJestTaLiczba());
//        wpis = wpis + this->getStalaUczenia() * (double)getErr() * (double) getCzyPrzykladJestTaLiczba();
        wpis = wpis + this->getStalaUczenia() * (double)getErr() * (double) pp.getMiejsce(i);
        localTablicaWag[i]=wpis;
//        qDebug("Po zmianie: %f", wpis);
    }
//    qDebug("Zmiana wartości theta: %f, ERR: %d, stalaUczenia: %f", this->getTheta(), this->getErr(), this->getStalaUczenia());
    this->setTheta(getTheta() - (double)getErr() * (double)getStalaUczenia() );
//    qDebug("Nowa theta: %f", this->getTheta());
    this->setTablicaWag(localTablicaWag);
}


/**
 * Metoda zlicza prawidłowo klasyfikowane przez dany perceptron przykłady (z tych niezaburzonych).
 */
int Perceptron::liczKlasyfikowanePrzyklady(QVector<Przyklad> lista){
//    qDebug("Liczymy klasyfikowane przykłady.");
    int wynik = 0;

    for(int i = 0; i < lista.size(); i++){
        Przyklad p = lista[i];
        this->wszystkieOperacje2(p);
        if(this->getErr() == 0){
            wynik++;
        }
    }

    return wynik;
}















#-------------------------------------------------
#
# Project created by QtCreator 2018-11-04T09:19:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUIperceptron2018
TEMPLATE = app


SOURCES += main.cpp\
        gui.cpp \
    przyklad.cpp \
    perceptron.cpp

HEADERS  += gui.h \
    przyklad.h \
    perceptron.h

FORMS    += gui.ui

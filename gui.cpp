#include "gui.h"
#include "ui_gui.h"
#include <QPainter>
#include <QPalette>
#include <przyklad.h>
#include <perceptron.h>
#include <QFile>
#include <QTextStream>
#include <QString>

/**
 * Klasa ta zawiera metody do GUI do perceptronu.
 *
 * 4 listopada 2018
 *
 * Rafał Kaczorkiewicz
 *
 * Mamy cztery główne przyciski, 10 pomocniczych (odpowiadają za poszczególne cyfry),
 * oraz główny obraz roboczy.
 *
 * Idea - wciśnięcie któregoś z głównych przycisków pociąga za sobą odpowiednią akcję z perceptronami.
 *
 * Na starcie obraz jest czarny. Zakładamy, że zapalenie superPixela (30x30 pikseli) jest pokazywane jako
 * wyświetlanie tego obszaru na biało.
 *
 * Trzeba jeszcze przemyśleć, czy dopuszczamy zapis wyklikanego przykładu do pliku.
 *
 * Oraz zapis wag do pliku.
 *
 * Możemy także dodać generowanie przykladów.
 *
 * EDIT:
 *  - dodajemy przycisk zapisu wag do pliku,            O
 *  - dodajemy przycisk wczytania wag z pliku,          O
 *  - dodajemy kieszonkę,                               v
 *  - dodajemy zapadkę.                                 v
 *
 */

GUI::GUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GUI)
{
    ui->setupUi(this);
    img = new QImage(151, 211, QImage::Format_RGB32);
    img->fill(Qt::black);

    czyJestTrybNauki = true;

    connect(ui->przyciskUczenie, SIGNAL(clicked()), this, SLOT(uczPerceptrony()));
    connect(ui->przyciskWczytaj, SIGNAL(clicked()), this, SLOT(wczytajPrzyklad()));
    connect(ui->przyciskLosuj, SIGNAL(clicked()), this, SLOT(losujPrzyklad()));
    connect(ui->przyciskExit, SIGNAL(clicked()), this, SLOT(close()));

    // testowo
    connect(ui->przycisk0, SIGNAL(clicked()), this, SLOT(przycisk0akcja()));
    connect(ui->przycisk1, SIGNAL(clicked()), this, SLOT(przycisk1akcja()));

    // przyciski zapisu i wczytania wag
    connect(ui->przyciskWczytajWagi, SIGNAL(clicked()), this, SLOT(wczytajWagi()));
    connect(ui->przyciskZapiszWagi, SIGNAL(clicked()), this, SLOT(zapiszWagi()));

    listaPerceptronow = QVector<Perceptron>();
    listaPrzykladow = QVector<Przyklad>();
    listaPrzykladowDoTestow = QVector<Przyklad>();
    dodajParePrzykladow();

}

GUI::~GUI()
{
    delete ui;
}

void GUI::paintEvent(QPaintEvent *){
    QPainter p(this);
    p.drawImage(10, 10, *img);
}

/**
  * Jeżeli NIE jesteśmy w trybie nauki, to możemy sami wyklikać cyfrę.
  * Jeśli dany kliknięty piksel jest już zapalony, to należy go zgasić.
  *
  */
void GUI::mousePressEvent(QMouseEvent *event){
//    qDebug("Przechwycono kliknięcie myszy.");
    if(!czyJestTrybNauki){
        zamalujSuperPixel(event->x(), event->y());
    }
    else{
        qDebug("Jesteśmy w trybie nauki, nie można zaznaczać pikseli.");
    }
}

/**
 * Metoda ta odpowiada za zapalenie pojedyńczego piksela. Sprawdza, czy jesteśmy w obszarze rysunku.
 */
void GUI::putPixel(int x, int y, QRgb color){
    if(x >= 0 && x < img->width() && y >= 0 && y < img->height()){
        img->setPixel(x, y, color);
    }
    else{
        qDebug("Jesteśmy poza obrazkiem.");
    }
    update();
}

/**
 * Metoda ta dostaje na wejściu współrzędne kliknięcia. Następnie szuka współrzędnych lewego górnego wierzchołka
 * superpiksela (30x30), w który kliknęliśmy.
 *
 * Pobiera także kolor z klikniętego piksela.
 *
 * Wreszcie maluje cały superpiksel na kolor przeciwny (czarny -> biały, biały -> czarny),
 * używając w pętli metody putPixel(*,*,*).
 */
void GUI::zamalujSuperPixel(int x, int y){
    QRgb color = img->pixel(x,y);
    QRgb colorBialy = qRgb(255,255,255);
    int xG = ( x/30 )* 30;
    int yG = ( y/30 )* 30;
//    qDebug("color = %d", color);
    if(color == colorBialy){
        //color = Qt::black;
        color = qRgb(0,0,0);
//        qDebug("Był biały, malujemy na czarno, %d - to on", color);
    }
    else{
        //color = Qt::white;
        color = qRgb(255,255,255);
//        qDebug("Był czarny, malujemy na biało, %d - to on, %d - ten drugi", color, Qt::black);
    }
//    qDebug("Współrzędne klikniecia: (%d, %d), współrzędne rogu: (%d, %d)", x, y, xG, yG);
    for(int i = 0; i < 30; i++){
        for(int j = 0; j < 30; j++){
            putPixel(xG + i, yG + j, color);
        }
    }
//    qDebug("Zamalowano superPixel 30x30.");
    update();
}

/**
 * Ta metoda dostaje jako parametry współrzędne lewego górnego wierzchołka superpiksela,
 * który zamalowujemy na podany kolor.
 */
void GUI::zamalujSuperPixel(int x, int y, QRgb kolor){
    for(int i = 0; i < 30; i++){
        for(int j = 0; j < 30; j++){
            putPixel(x + i, y + j, kolor);
        }
    }
    update();
}

/**
 * Metoda ta dostaje na wejściu Przyklad(), a następnie interpretuje go jako rysunek
 * i wyświetla na img.
 *
 * każdą kolejną cyfrę interpretujemy jako zapalenie (1) lub zgaszenie (0) superpiksela
 *
 */
void GUI::rysujRysunek(Przyklad p){
//    qDebug("\n Rysujemy rysunek \n");
//    QRgb kolor = qRgb(255,255,255);
    int x = 0;
    int y = 0;
    int j = 0;
    for(int i = 0; i < p.dlugoscPrzykladu(); i++){
        // liczymy numery wiersza i kolumny wierzchołka superpiksela
        x = (i % 5) * 30;
        if(x == 0 && i>0){
            j++;
        }
        y = j * 30;
        if(p.getMiejsce(i) == 1){
            // gasimy superpiksel
//            zamalujSuperPixel(x, y, kolor);
            zamalujSuperPixel(x, y, qRgb(255,255,255));
//            qDebug("Zapalamy superpixel (%d, %d)", x,y);
        }
        else{
            // zapalamy superpiksel
 //           kolor = qRgb(0,0,0);
 //           zamalujSuperPixel(x, y, kolor);
            zamalujSuperPixel(x, y, qRgb(0,0,0));
//            qDebug("gasimy superpixel (%d, %d)", x,y);
        }
    }
    update();
}

/**
 * Poniższa metoda odpowiada za przepuszczenie danego przykladu, podanego jako parametr wywołania,
 * przez naszą sieć nauronową.
 *
 * Jako wynik zapala przyciski.
 */
void GUI::sprawdzPrzyklad(Przyklad &p){
    czysc();
    rysujRysunek(p);
//    p.wypisz();
    for(int i = 0; i < listaPerceptronow.size(); i++){
        Perceptron per = listaPerceptronow.at(i);
        per.wszystkieOperacje2(p);
//        per.wypiszTabliceWag();
//        qDebug("Wynik działania sieci: %f", per.getWynikDzialaniaSieci());
        if(/*per.getErr() == 0 && */per.getWynikDzialaniaSieci() > 0){
//            qDebug("rozpoznano: %d", per.getN());
            switch (per.getN()){
                case 0:
                    zapal0();
                    break;
                case 1:
                    zapal1();
                    break;
                case 2:
                    zapal2();
                    break;
                case 3:
                    zapal3();
                    break;
                case 4:
                    zapal4();
                    break;
                case 5:
                    zapal5();
                    break;
                case 6:
                    zapal6();
                    break;
                case 7:
                    zapal7();
                    break;
                case 8:
                    zapal8();
                    break;
                case 9:
                    zapal9();
                    break;
            default:
                zapalPytaj();
                break;
            }
        }
    }
//    qDebug("Przykład został sprawdzony. \n");
}

/**
 * Metoda ta odpowiada za stworzenie dwóch list przykładów - jednej do nauki,
 * drugiej do testowania.
 */
void GUI::dodajParePrzykladow(){
    qDebug("Dodajemy przykłady do nauki i testów.");
    Przyklad p0 = Przyklad(0);
    p0.dodajDoListy("01110100011000110001100011000101110", 0);
    listaPrzykladow.push_back(p0);
    Przyklad p1 = Przyklad(1);
    p1.dodajDoListy("00001000110010101001100010000100001", 1);
    listaPrzykladow.push_back(p1);
    Przyklad p2 = Przyklad(2);
    p2.dodajDoListy("01110100010001000100010001000011111", 2);
    listaPrzykladow.push_back(p2);
    Przyklad p3 = Przyklad(3);
    p3.dodajDoListy("11110000010000101111000010000111110", 3);
    listaPrzykladow.push_back(p3);
    Przyklad p4 = Przyklad(4);
    p4.dodajDoListy("00001000110010101001111110000100001", 4);
    listaPrzykladow.push_back(p4);
    Przyklad p5 = Przyklad(5);
    p5.dodajDoListy("11111100001000011110000010000111110", 5);
    listaPrzykladow.push_back(p5);
    Przyklad p6 = Przyklad(6);
    p6.dodajDoListy("11111100001000011111100011000111110", 6);
    listaPrzykladow.push_back(p6);
    Przyklad p7 = Przyklad(7);
    p7.dodajDoListy("11111000010001000100010001000010000", 7);
    listaPrzykladow.push_back(p7);
    Przyklad p8 = Przyklad(8);
    p8.dodajDoListy("01110100011000101110100011000101110", 8);
    listaPrzykladow.push_back(p8);
    Przyklad p9 = Przyklad(9);
    p9.dodajDoListy("11111100011000101111000010000101111", 9);
    listaPrzykladow.push_back(p9);

    Przyklad p00 = Przyklad(0);
    p00.dodajDoListy("01110100011000110001100011000101110", 0);
    listaPrzykladowDoTestow.push_back(p00);
    Przyklad p11 = Przyklad(1);
    p11.dodajDoListy("00001100110010101001101010000100001", 1);
    listaPrzykladowDoTestow.push_back(p11);
    Przyklad p22 = Przyklad(2);
    p22.dodajDoListy("01110100010001000100011000000011111", 2);
    listaPrzykladowDoTestow.push_back(p22);
    Przyklad p33 = Przyklad(3);
    p33.dodajDoListy("01110100010000101110010011000101110", 3);
    listaPrzykladowDoTestow.push_back(p33);
    Przyklad p44 = Przyklad(4);
    p44.dodajDoListy("00001000100010001001111110000100001", 4);
    listaPrzykladowDoTestow.push_back(p44);
    Przyklad p55 = Przyklad(5);
    p55.dodajDoListy("11111100001000010000111100000111110", 5);
    listaPrzykladowDoTestow.push_back(p55);
    Przyklad p66 = Przyklad(6);
    p66.dodajDoListy("01111100001000011110100011000111111", 6);
    listaPrzykladowDoTestow.push_back(p66);
    Przyklad p77 = Przyklad(7);
    p77.dodajDoListy("11111000010001011111001000100010000", 7);
    listaPrzykladowDoTestow.push_back(p77);
    Przyklad p88 = Przyklad(8);
    p88.dodajDoListy("01110100011000101110100011000111111", 8);
    listaPrzykladowDoTestow.push_back(p88);
    Przyklad p99 = Przyklad(9);
    p99.dodajDoListy("01110100011000101110000010000101110", 9);
    listaPrzykladowDoTestow.push_back(p99);
    Przyklad p01 = Przyklad(-1);
    p01.dodajDoListy("00010001010100110100000100000111111", -1);
    listaPrzykladowDoTestow.push_back(p01);
    Przyklad p02 = Przyklad(-1);
    p02.dodajDoListy("00100010111000101010001000101010101", -1);
    listaPrzykladowDoTestow.push_back(p02);
    Przyklad p03 = Przyklad(-1);
    p03.dodajDoListy("10001010100010010001100010000001110", -1);
    listaPrzykladowDoTestow.push_back(p03);
    qDebug("Dodaliśmy przykłady.");
}

/**
 * Metoda dostaje na wejściu długość listy.
 *
 * Następnie losujemy liczbę z podanego zakresu i zwracamy ją.
 */
int GUI::losujNumerPrzykladu(int dlugosc){
    int n = rand();
    n = n % dlugosc;
//    qDebug("Losowanie 1-arg, wylosowaliśmy: %d", n);
    return n;
}

/**
 * Metoda dostaje na wejściu długość tablicy, z której bedzie losować,
 * oraz cyfrę, jaką ma wylosować.
 * Zwraca numer przykładu.
 *
 * Jeżeli los > 1/3, to losujemy cyfrę, w p.p. zwracamy liczbę,
 * na którym miejscu jest dana cyfra, rozpoznawana przez ten perceptron.
 */
int GUI::losujNumerPrzykladu(int dlugosc, int cyfra){
    int wynik = 0;
    double n = (double) rand()/RAND_MAX;
    if(n < 1/3.0){
        wynik = cyfra;
    }
    else{
        wynik = rand() % dlugosc;
    }
//    qDebug("Losowanie 2 arg, wylosowaliśmy: %d", wynik);
        return wynik;
}

/**
 * Drugie podejście do problemu.
 *
 * Dostajemy na starcie przykład, losujemy liczbę z przedziału (0,35), zamieniamy piksel na przeciwny.
 *
 * 14 listopada 2018
 *
 */
Przyklad GUI::zaburzPrzyklad(Przyklad p){
//    qDebug("Metoda zaburzająca przykład.");
//    p.wypisz();
    int ktoryPikselOdwracamy = rand() % 35;
    QVector<int> listaPomocnicza = QVector<int>();
    listaPomocnicza = p.getLista();
    //int i = wynik.at(ktoryPikselOdwracamy);
    int i = listaPomocnicza.at(ktoryPikselOdwracamy);
//    qDebug("Odwracamy %d piksel, mamy tam: %d", ktoryPikselOdwracamy, i);
    if(i == 0){
        i = 1;
    }
    else{
        i = 0;
    }
//    qDebug("Po zamianie mamy tam: %d", i);
    listaPomocnicza.replace(ktoryPikselOdwracamy,i);
    p.setLista(listaPomocnicza);
//    p.wypisz();
    return p;
}


/**
 * Metoda czyści wszystkie podświetlenia przycisków. Najpierw czyści obraz, tj wypełnia go na czarno.
 */
void GUI::czysc(){
    img->fill(Qt::black);
    zgas0();
    zgas1();
    zgas2();
    zgas3();
    zgas4();
    zgas5();
    zgas6();
    zgas7();
    zgas8();
    zgas9();
    zgasPytaj();
//    qDebug("Wyczyściliśmy podświetlenie przycisków.");
}

/**
 * Metoda odpowiada za zmianę koloru przycisku 0.
 */
void GUI::zapal0(){
    QPalette pal = ui->przycisk0->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk0->setAutoFillBackground(true);
    ui->przycisk0->setPalette(pal);
    ui->przycisk0->update();
}

void GUI::zapal1(){
    QPalette pal = ui->przycisk1->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk1->setAutoFillBackground(true);
    ui->przycisk1->setPalette(pal);
    ui->przycisk1->update();
}

void GUI::zapal2(){
    QPalette pal = ui->przycisk2->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk2->setAutoFillBackground(true);
    ui->przycisk2->setPalette(pal);
    ui->przycisk2->update();
}

void GUI::zapal3(){
    QPalette pal = ui->przycisk3->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk3->setAutoFillBackground(true);
    ui->przycisk3->setPalette(pal);
    ui->przycisk3->update();
}

void GUI::zapal4(){
    QPalette pal = ui->przycisk4->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk4->setAutoFillBackground(true);
    ui->przycisk4->setPalette(pal);
    ui->przycisk4->update();
}

void GUI::zapal5(){
    QPalette pal = ui->przycisk5->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk5->setAutoFillBackground(true);
    ui->przycisk5->setPalette(pal);
    ui->przycisk5->update();
}

void GUI::zapal6(){
    QPalette pal = ui->przycisk6->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk6->setAutoFillBackground(true);
    ui->przycisk6->setPalette(pal);
    ui->przycisk6->update();
}

void GUI::zapal7(){
    QPalette pal = ui->przycisk7->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk7->setAutoFillBackground(true);
    ui->przycisk7->setPalette(pal);
    ui->przycisk7->update();
}

void GUI::zapal8(){
    QPalette pal = ui->przycisk8->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk8->setAutoFillBackground(true);
    ui->przycisk8->setPalette(pal);
    ui->przycisk8->update();
}

void GUI::zapal9(){
    QPalette pal = ui->przycisk9->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przycisk9->setAutoFillBackground(true);
    ui->przycisk9->setPalette(pal);
    ui->przycisk9->update();
}

void GUI::zapalPytaj(){
    QPalette pal = ui->przyciskPytaj->palette();
    pal.setColor(QPalette::Button, QColor(Qt::yellow));
    ui->przyciskPytaj->setAutoFillBackground(true);
    ui->przyciskPytaj->setPalette(pal);
    ui->przyciskPytaj->update();
}

void GUI::zgas0(){
     ui->przycisk0->setAutoFillBackground(false);
     ui->przycisk0->update();
}

void GUI::zgas1(){
    ui->przycisk1->setAutoFillBackground(false);
    ui->przycisk1->update();
}

void GUI::zgas2(){
    ui->przycisk2->setAutoFillBackground(false);
    ui->przycisk2->update();
}

void GUI::zgas3(){
    ui->przycisk3->setAutoFillBackground(false);
    ui->przycisk3->update();
}

void GUI::zgas4(){
    ui->przycisk4->setAutoFillBackground(false);
    ui->przycisk4->update();
}

void GUI::zgas5(){
    ui->przycisk5->setAutoFillBackground(false);
    ui->przycisk5->update();
}

void GUI::zgas6(){
    ui->przycisk6->setAutoFillBackground(false);
    ui->przycisk6->update();
}

void GUI::zgas7(){
    ui->przycisk7->setAutoFillBackground(false);
    ui->przycisk7->update();
}

void GUI::zgas8(){
    ui->przycisk8->setAutoFillBackground(false);
    ui->przycisk8->update();
}

void GUI::zgas9(){
    ui->przycisk9->setAutoFillBackground(false);
    ui->przycisk9->update();
}

void GUI::zgasPytaj(){
    ui->przyciskPytaj->setAutoFillBackground(false);
    ui->przyciskPytaj->update();
}

/**
 * Po wywołaniu tej metody rozpoczyna się nauka perceptronów, na podstawie algorytmu z zajęć i mojego kodu z javy.
 *
 * Najpierw dodajemy perceptrony do listy, później każdy perceptron z listy uczymy.
 *
 * Jest to główna metoda tej klasy.
 */
void GUI::uczPerceptrony(){
//    qDebug("Zaczynamu uczyć perceptrony");
    czyJestTrybNauki = true;
//    qDebug("test0");
    if(listaPerceptronow.empty()){
        qDebug("Pusta lista perceptronów.");
        for(int cyfra = 0; cyfra < 10; cyfra++){
          Perceptron per = Perceptron(cyfra);  //       poniższe dla testów jednego perceptrona
        //    Perceptron per = Perceptron(0);
            listaPerceptronow.push_back(per);
        //    qDebug("Stworzyliśmy %i perceptron",cyfra);
        }
    }
//    qDebug("Pora uczyć stworzone perceptrony.");
    for(int i  = 0; i < listaPerceptronow.size(); i++){
        uczPerceptron(listaPerceptronow[i]);
        qDebug("Nauczyliśmy %d perceptron", i);
    }
    czyJestTrybNauki = false;
}


void GUI::uczPerceptron(Perceptron &per){
    int zapadka = 0;
    int maxZapadka = 0;
    int czasZyciaWag = 0;
    int czasZyciaRekordzisty = 0;
    double thetaKieszonka = per.getTheta();
    QVector<double> wagiKieszonka = per.getTablicaWag();
//    QVector<double> tablicaTheta = QVector<double>();
//    qDebug("Uczymy %d perceptron: stalaUczenia: %f, theta: %f", per.getN(), per.getStalaUczenia(), per.getTheta());
    int licznik = 0;
    bool czyJeszczeSprawdzamy = true;
    while(czyJeszczeSprawdzamy){
        int numerPrzykladu = losujNumerPrzykladu(listaPrzykladow.size(), per.getN());
        Przyklad rozpatrywany = listaPrzykladow.at(numerPrzykladu);
        rozpatrywany = zaburzPrzyklad(rozpatrywany);
//        qDebug("\n Rozpatrujemy: %d", rozpatrywany.getJakaJestCyfra());
        per.wszystkieOperacje2(rozpatrywany);
//        tablicaTheta.push_back(per.getTheta());
        licznik++;
        if(per.getErr() == 0){
            // dany przykład został rozpoznany poprawnie, przechodzimy do kolejnego
            czasZyciaWag++;
            zapadka = per.liczKlasyfikowanePrzyklady(listaPrzykladow);
            if(czasZyciaWag > czasZyciaRekordzisty && zapadka >= maxZapadka){
                // jeśli czas życia wag jest lepszy od wcześniejszego, to podmieniamy - nasza kieszonka
                // do tego jeśli klasyfikujemy więcej, niż wcześniej - nasza zapadka
//                qDebug("Podmieniamy wagi w kieszonce.");
                czasZyciaRekordzisty = czasZyciaWag;
                wagiKieszonka = per.getTablicaWag();
                maxZapadka = zapadka;
                thetaKieszonka = per.getTheta();
            }
        }
        else {
            // przykład nie został poprawnie rozpoznany, uaktualniamy wagi
//            qDebug("NIE rozpoznaliśmy poprawnie przykładu - podmieniamy wagi.");
            czasZyciaWag = 0;
            per.uaktualnijWagi(rozpatrywany);
        }
        //qDebug("zaburzamy wylosowany przykład");
        if(licznik == 1000){
            czyJeszczeSprawdzamy = false;
        }

    }
    // najbardziej żywotny zestaw wag
    qDebug("\n Czas życia rekordzisty: %d, zapadka: %d", czasZyciaRekordzisty, maxZapadka);
    per.setTablicaWag(wagiKieszonka);
    per.setTheta(thetaKieszonka);
    // wypisujemy wagi z kieszonki - zeby sprawdzić, czy dobre tam trafiają:
//    for(int i =0; i < wagiKieszonka.size(); i++){
//        qDebug(" %f ", wagiKieszonka.at(i));
//    }
//    qDebug("Końcowe wagi (te najlepsze, ha ha):");
//    per.wypiszTabliceWag();
//    qDebug("Koncowa suma dla cyfry, jaką powinien rozpoznawać ten perceptron:");
//    per.wszystkieOperacje2(listaPrzykladow.at(per.getN()));
/*    qDebug("Wszystkie thety, czy jest wśród nich zmniejszenie?");
    for(int i = 0; i < tablicaTheta.size(); i++){
        //qDebug("%f", tablicaTheta.at(i));
        if( i < tablicaTheta.size() - 1 ){
            double i1 = tablicaTheta.at(i);
            double i2 = tablicaTheta.at(i+1);
            if( i1 > i2){
                qDebug("Zmniejszamy theta, %d, %f", i, tablicaTheta[i]);
            }
            if(i1 < i2){
                qDebug("Rośnie: %f", tablicaTheta[i]);
            }
        }
    }*/
}

// funkcja błędów
// jeżeli getErr jest różne od 0, to zliczamy
int GUI::funkcjaBledow(Perceptron per){
    qDebug("Wywołaliśmy funkcję błędów.");
    int licznik = 0;
    for(int i = 0; i < listaPrzykladow.size(); i++){
        Przyklad p = listaPrzykladow.at(i);
        per.coJestNaWyjsciu(p);
        per.wartoscERR(p.getJakaJestCyfra());
        if(per.getErr() != 0){
            licznik++;
        }

    }
    return licznik;
}

/**
 * Po wywołaniu tej metody wczytujemy przykład wyswietlany obecnie jako img.
 * Następnie wywołujemy na nim metodę: sprawdzPrzyklad, która nam zapali te przyciski cyfrowe,
 * jakie cyfry rozpozna w naszym obrazku.
 *
 * Przykład ten jest traktowany jako -1, czyli że nie przedstawia żadnej cyfry. Dla naszego porównania nie ma to znaczenia.
 */
void GUI::wczytajPrzyklad(){
//    qDebug("Rozpoczynamy wczytywanie przykładu");
    if(czyJestTrybNauki){
       return;
    }
//    czysc();
    Przyklad p = Przyklad(-1);
    bool k;
    for(int i = 0; i < img->height()-1; i = i + 30){
        for(int j = 0; j < img->width()-1; j = j + 30){
            if(k = (img->pixel(j,i) == qRgb(0,0,0))){
                p.dodajDoListy(0);
//                qDebug("Kolor 0");
            }
            else{
                if(img->pixel(j,i) != qRgb(255,255,255)){
//                    qDebug("Inny kolor");
                }
                else{
                    p.dodajDoListy(1);
//                    qDebug("Kolor 1");
                }
            }
//            qDebug("(%d, %d) %d",j,i, k? 0 : 1);
        }
    }
    sprawdzPrzyklad(p);
//    img->fill(Qt::black);
    update();
//    rysujRysunek(p);
//    qDebug("Zakończyliśmy wczytywanie przykłady");
}

/**
 * Metoda ta wylosuje przykład z listy przykładów testowych, a nstępnie wywoła na nim metodę sprawdzPrzyklad()
 * i wynik zwróci w postaci zapalonych przycisków.
 */
void GUI::losujPrzyklad(){
    if(czyJestTrybNauki){
       return;
    }
//    qDebug("Losujemy przykład.");
    czysc();
    int numerPrzykladu = losujNumerPrzykladu(listaPrzykladowDoTestow.size());
    Przyklad wylosowany = listaPrzykladowDoTestow.at(numerPrzykladu);
    // rysujemy przykład na img
    wylosowany = zaburzPrzyklad(wylosowany);
    rysujRysunek(wylosowany);
    for(int i = 0; i < listaPerceptronow.size(); i++){
        Perceptron per = listaPerceptronow.at(i);
        // sprawdzPrzyklad(per);
        // ewentualnie jak w kodzie z javy:
        per.wszystkieOperacje2(wylosowany);
        if(/*per.getErr() == 0 &&*/ per.getWynikDzialaniaSieci() > 0){
//            qDebug("Rozpoznano: %d",per.getN());
            switch (per.getN()){
                case 0:
                    zapal0();
                    break;
                case 1:
                    zapal1();
                    break;
                case 2:
                    zapal2();
                    break;
                case 3:
                    zapal3();
                    break;
                case 4:
                    zapal4();
                    break;
                case 5:
                    zapal5();
                    break;
                case 6:
                    zapal6();
                    break;
                case 7:
                    zapal7();
                    break;
                case 8:
                    zapal8();
                    break;
                case 9:
                    zapal9();
                    break;
            default:
                zapalPytaj();
                break;
            }
        }
    }
    //    qDebug("Zakończyliśmy wczytywanie przykładu");
}

/**
 * Metoda obsługuje przycisk zapisu wag do pliku, na podstawie dokumentacji Qt.
 */
void GUI::zapiszWagi(){
    qDebug("Zapisujemy wagi.");
/*    int i = 0;
    QVector<double> listapomocnicza = QVector<double>();
    bool czyJeszcze = true;
    qDebug("Zapisz wagi().");
    QFile file("wagi.txt");
    if(czyJestTrybNauki){
        qDebug("ZapiszWagi() - jest tryb nauki, więc nie mamy wag jeszcze.");
       return;
    }
    if(! file.open(QIODevice::WriteOnly | QIODevice::Text)){
        qDebug("Zapisz wagi - błąd dostępu do pliku");
        return;
    }
    while(czyJeszcze){
        listapomocnicza = listaPerceptronow[i].getTablicaWag();
        for(int l = 0; l < listapomocnicza.size(); l++){
            file.write(listapomocnicza.at(i));

        }
        i++;
        if(i >= 10){
            czyJeszcze = false;
        }

    }
    file.close();
    qDebug("Zapisalismy do pliku.");*/
}

/**
 * Metoda obsługuje przycisk odczytu wag z pliku, na podstawie dokumentacji Qt.
 */
void GUI::wczytajWagi(){
    qDebug("Wczytaj wagi()."); /*
    int i = 0;
    QVector<double> tablicaPomocnicza = QVector<double>();
    QFile file("wagi.txt");
    if(! file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug("Wczytaj wagi - błąd dostępu do pliku");
        return;
    }
    QTextStream in(&file);
    while(!in.atEnd()){
        QString linia = in.readLine();
        tablicaPomocnicza.push_back(linia.toDouble());
        if(tablicaPomocnicza.size() == 35){
            listaPerceptronow[i].setTablicaWag(tablicaPomocnicza);
            i++;
            qDebug("Dodaliśmy %d wagi.", i-1);
        }
    }
    file.close();
    qDebug("Wczytaliśmy zawartość pliku.");*/
}

// poniższe metody nie są potrzebne, bo nie przewidujemy obsługę tych przycisków pomocniczych
void GUI::przycisk0akcja(){
    qDebug("Rysujemy zero - test metody rysującej");
    Przyklad zero = listaPrzykladow.at(0);
    rysujRysunek(zero);
    qDebug("Narysowano 0 z listy, co rozpoznało?");
}

void GUI::przycisk1akcja(){
    qDebug("Rysujemy jedynkę - test metody rysującej");
    Przyklad zero = listaPrzykladow.at(1);
    rysujRysunek(zero);
    qDebug("Narysowano 1 z listy, co rozpoznało?");
}

void GUI::przycisk2akcja(){

}

void GUI::przycisk3akcja(){

}

void GUI::przycisk4akcja(){

}

void GUI::przycisk5akcja(){

}

void GUI::przycisk6akcja(){

}

void GUI::przycisk7akcja(){

}

void GUI::przycisk8akcja(){

}

void GUI::przycisk9akcja(){

}

#include "przyklad.h"
#include <stdlib.h>
#include <stdio.h>
#include <QString>
#include <QVector>

/**
 * Na podstawie mojego kodu napisanego w javie.
 *
 * Rafał Kaczorkiewicz
 * 8 października 2018,
 * 4 listopada 2018
 *
 * konstruktor bezargumentowy używamy, jeżeli podany przykład nie jest żadną cyfrą
 * oraz dla wyklikiwanych przykładów
 */

Przyklad::Przyklad(){
    lista = QVector<int>();
    jakaJestCyfra = -1;
}

Przyklad::Przyklad(int cyfra){
    lista = QVector<int>();
    jakaJestCyfra = cyfra;
}

void Przyklad::setJakaJestCyfra(int jakaJestCyfra){
    this->jakaJestCyfra = jakaJestCyfra;
}

int Przyklad::getJakaJestCyfra(){
    return jakaJestCyfra;
}

QVector<int> Przyklad::getLista(){
    return lista;
}

void Przyklad::setLista(QVector<int> lista){
    this->lista = lista;
}

int Przyklad::dlugoscPrzykladu(){
    return lista.size();
}

int Przyklad::getMiejsce(int miejsce){
    return lista.at(miejsce);
}

void Przyklad::dodajDoListy(int i){
    lista.push_back(i);
}

void Przyklad::wyczyscListeAbyStworzycKolejnyPrzyklad(){
    lista.clear();
}

/**
 * metoda wypisuje na ekranie dany przykład
 */
void Przyklad::wypisz(){
    for(int i = 0; i < lista.size(); i++){
        qDebug(" %d ", lista.at(i));
//        printf(" %d ", i);
    }
}

/**
 * Metoda bierze jako parametr string oraz inta oznaczającego, jaką cyfrę przedstawia napis.
 * Zwraca tablicę intów.
 */
Przyklad Przyklad::dodajDoListy(QString napis, int cyfra){
//    qDebug("Zaczynamy dodawać przykład do listy.");
    Przyklad wynik = Przyklad(cyfra);

    for(int i = 0; i < napis.size() ; i++){
        int l;
        // potencjalnie poniżej jest słaby punkt, odejmujemy 48 jako przesunięcie w unicode
        l = (int) napis.at(i).unicode() - 48;
        //qDebug("%d", l);
        lista.push_back(l);
    }
//    qDebug("Dodaliśmy przykład do listy");
    return wynik;
}

/**
 * Metoda ma na celu zaburzenie danego przykładu.
 * Przyjmuje przykład, losuje liczbę i zamienia bit na danym miejscu na przeciwny
 * (czyli odwraca dany piksel).
 *
 * dodana 14 listopada 2018
 *
 * chyba muszę przenieść tą metodę do głównej klasy, bo w tym miejscu się za bardzo kopie

void Przyklad::zaburzPrzyklad(){
//Przyklad Przyklad::zaburzPrzyklad(){
    this->wypisz();
    int ktoryPikselOdwracamy = rand() % 35;
    QVector<int> *listaPomocnicza = this->getLista();
    //int i = wynik.at(ktoryPikselOdwracamy);
    int i = listaPomocnicza->at(i);
    qDebug("Odwracamy %d piksel, mamy tam: %d", ktoryPikselOdwracamy, i);
    if(i == 0){
        i = 1;
    }
    else{
        i = 0;
    }
    qDebug("Po zamianie mamy tam: %d", i);
    listaPomocnicza->replace(ktoryPikselOdwracamy,i);
    this->setLista(listaPomocnicza);
    this->wypisz();
//    return this;
}
*/

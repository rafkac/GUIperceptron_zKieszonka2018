#ifndef GUI_H
#define GUI_H

#include <QMainWindow>
#include <QPushButton>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QVector>
#include <przyklad.h>
#include <perceptron.h>

namespace Ui {
class GUI;
}

class GUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit GUI(QWidget *parent = 0);
    ~GUI();
    void uczPerceptron(Perceptron &per);

private:
    Ui::GUI *ui;

    QImage *img;
    QVector<Przyklad> listaPrzykladow;
    QVector<Przyklad> listaPrzykladowDoTestow;

    QVector<Perceptron> listaPerceptronow;

    bool czyJestTrybNauki;
    // jeśli true - to się uczymy, jeśli false - to nauczeni,
    // ucząc się nie możemy wyklikiwać przykładów

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);

    void putPixel(int x, int y, QRgb color);

    void zamalujSuperPixel(int x, int y);
    void zamalujSuperPixel(int x, int y, QRgb kolor);
    void rysujRysunek(Przyklad p);                        // docelowo bedzie tutaj jako paramter przyklad z klasy Przykład

    void sprawdzPrzyklad(Przyklad &p);

    void dodajParePrzykladow();

    int losujNumerPrzykladu(int dlugosc);
    int losujNumerPrzykladu(int dlugosc, int cyfra);

    Przyklad zaburzPrzyklad(Przyklad p);

    void czysc();

    // teraz metody odpowiedzialne za zapalanie odpowiednich przycisków
    void zapal0();
    void zapal1();
    void zapal2();
    void zapal3();
    void zapal4();
    void zapal5();
    void zapal6();
    void zapal7();
    void zapal8();
    void zapal9();
    void zapalPytaj();

    // i za gaszenie
    void zgas0();
    void zgas1();
    void zgas2();
    void zgas3();
    void zgas4();
    void zgas5();
    void zgas6();
    void zgas7();
    void zgas8();
    void zgas9();
    void zgasPytaj();

    int funkcjaBledow(Perceptron per);
public slots:
    void uczPerceptrony();
    void wczytajPrzyklad();
    void losujPrzyklad();
    void zapiszWagi();
    void wczytajWagi();
    void przycisk0akcja();
    void przycisk1akcja();
    void przycisk2akcja();
    void przycisk3akcja();
    void przycisk4akcja();
    void przycisk5akcja();
    void przycisk6akcja();
    void przycisk7akcja();
    void przycisk8akcja();
    void przycisk9akcja();
};

#endif // GUI_H
